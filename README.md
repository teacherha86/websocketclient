# Simple Echo Client


## Pre Install Packages
### [NativeWebSocket](https://github.com/endel/NativeWebSocket)
- Install via UPM (Unity Package Manager)
1. Open Unity
2. Open Package Manager Window
3. Click Add Package From Git URL
4. Enter URL: `https://github.com/endel/NativeWebSocket.git#upm`



## UPM Package
### Install via git URL
1. Open Windows -> Package Manager
2. add package from Git URL
3. enter `https://gitlab.com/teacherha86/websocketclient.git?path=Assets/EchoWebsocket`