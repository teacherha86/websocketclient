using Newtonsoft.Json;
using System.Collections.Generic;

namespace NativeWebSocket.Extentions
{
    public class User 
    {
        [JsonProperty("uid")]
        public string UID;     
        
        public void Copy (in User from)
        {
            this.UID = from.UID;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class Room
    {
        [JsonProperty("roomId")]
        string roomId;

        [JsonProperty("maxUser")]
        int maxUser;

        [JsonProperty("users")]
        readonly public List<User> Users = new List<User>();


        public string RoomId => roomId;
        public int MaxUser => maxUser;

        public int JoinedUserLen => Users.Count;

        public void AddUser(in User user)
        {
            Users.Add(user);
        }
        
        public void LeftUser (string uid)
        {
            var getIndex = Users.FindIndex((u => u.UID == uid));
            if (0 <= getIndex)
                Users.RemoveAt(getIndex);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
