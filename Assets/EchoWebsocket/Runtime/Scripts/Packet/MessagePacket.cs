using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace NativeWebSocket.Extentions
{
    public enum MessagePacketCastType
    {
        All = 0,
        Others
    }

    public struct MessagePacketReq
    {
        [JsonProperty("castType")]
        public MessagePacketCastType CastType;
        [JsonProperty("message")]
        public string Message;

        public MessagePacketReq(string message, MessagePacketCastType castType = MessagePacketCastType.All)
        {
            CastType = castType;
            Message = message;
        }
    }



    #region Message Response Packets

    public enum MessagePacketType
    {
        None,
        Join,
        Left,
        Full,
        Message,
        RoomInfo
    }

    public class MessagePacketTypeResp
    {
        [JsonProperty("messageType"), JsonConverter(typeof(StringEnumConverter))]
        public MessagePacketType MessageType;
    }


    public class MessagePacketResp : MessagePacketTypeResp
    {
        [JsonProperty("fromUid")]
        public string FromUid;
        [JsonProperty("message")]
        public string Message;        
    }
    public class JoinPacketResp : MessagePacketTypeResp
    {
        [JsonProperty("joinTime")]
        public DateTime JoinTime;
        [JsonProperty("user")]
        public User User;
    }

    public class LeftPackekResp : MessagePacketTypeResp
    {
        [JsonProperty("leftTime")]
        public DateTime LeftTime;
        [JsonProperty("user")]
        public User User;
    }

    public class RoomInfoPacketResp : MessagePacketTypeResp
    {
        [JsonProperty("myInfo")]
        public User Mine;
        [JsonProperty("room")]
        public Room Room;
    }


    public struct MessagePacket
    {
        public string Type;
        public string Message;
        public string FromUid;

        static public MessagePacket ParseFromMessage (string message)
        {
            try
            {
                return JsonConvert.DeserializeObject<MessagePacket>(message);
            }
            catch 
            {
                return default;
            }
        }

        public MessagePacket (string type, string message, string fromUid)
        {
            Type = type;
            Message = message;
            FromUid = fromUid;
        }

        public T ToObject<T>()
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(Message);
            }
            catch
            {
                return default;
            }
        }

        public override string ToString()
        {
            return Message;
        }

        public string ToJson ()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    #endregion

}
