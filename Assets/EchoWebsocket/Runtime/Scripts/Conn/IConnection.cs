namespace NativeWebSocket.Extentions
{
    using System;
    using System.Threading.Tasks;

    public interface IConnection
    {
        string UID { get; }
        bool IsConnected { get; }
        Room JoinedRoom { get; }
        bool IsMaster { get; }
        WebSocketState State { get; }
        Settings Settings { get; }

        event Action OnOpen;
        event Action<WebSocketCloseCode> OnClose;
        event Action<string> OnError;
        event Action<string> OnJoined;
        event Action<string> OnLeft;
        event Action<MessagePacket> OnMessage;

        void Connect();
        void Connect(Settings settings);
        void Close();
        Task Send(string type, object obj, MessagePacketCastType castType = MessagePacketCastType.All);
        Task Send(MessagePacket packet, MessagePacketCastType castType = MessagePacketCastType.All);
    }
}
