using System;
using System.Threading.Tasks;
using UnityEngine;

namespace NativeWebSocket.Extentions
{

    [Serializable]
    public struct Settings
    {
        public string RemoteIp;
        public int Port;
        public string RoomId;
        [Min(2)]public int MaxPlayer;

        static public Settings Default ()
        {
            return new Settings
            {
                RemoteIp = "ws://localhost",
                Port = 8080,
                RoomId = "room number",
                MaxPlayer = 100000,
            };
        }

        readonly public string Uri () => $"{RemoteIp}:{Port}/echo/{RoomId}?max={MaxPlayer}";
    }

    public class EchoConnection : MonoBehaviour, IConnection
    {
        static EchoConnection s_instance = null;
        static public IConnection Instance
        {
            get
            {
                if(s_instance == null)
                    s_instance = GameObject.FindObjectOfType<EchoConnection>() ;
                return s_instance;
            }
        }

        readonly User mine = new User();
        Room m_JoinedRoom = new Room();

        [SerializeField]  Settings m_Settings = Settings.Default();
        WebSocket websocket;

        public WebSocketState State => websocket != null ? websocket.State : WebSocketState.Closed;

        public event Action OnOpen;
        public event Action<WebSocketCloseCode> OnClose;
        public event Action<string> OnError;
        public event Action<string> OnJoined;
        public event Action<string> OnLeft;
        public event Action<MessagePacket> OnMessage;


        public string UID => mine.UID;
        public Room JoinedRoom => m_JoinedRoom;

        public Settings Settings => m_Settings;

        public bool IsConnected => State == WebSocketState.Open;

        public bool IsMaster => IsConnected && JoinedRoom.Users[0].UID == UID;

        private void OnDestroy()
        {
            if (s_instance != null && s_instance == this)
                s_instance = null;
            websocket?.Close();
            websocket = null;
        }


        void Awake ()
        {
            if (s_instance == null || s_instance == this)
            {
                s_instance = this;
                DontDestroyOnLoad(gameObject);
            } 
            else
            {          
                Destroy(gameObject);
            }
        }

        async public void Connect()
        {
            await InternalConnect();
        }

        async public void Connect(Settings settings)
        {
            m_Settings = settings;
            await InternalConnect();
        }

        async Task InternalConnect()
        {
            if (IsConnected)
                Close();
            websocket = new WebSocket(m_Settings.Uri());
            // websocket.OnOpen += () => OnOpen?.Invoke();
            websocket.OnError += (e) => OnError?.Invoke(e);
            websocket.OnClose += (e) => OnClose?.Invoke(e);
            websocket.OnMessage += (bytes) =>
            {
                var msg = TextParser.Parse(bytes);
                var msgType = JsonParser.Parse<MessagePacketTypeResp>(msg);
                switch (msgType.MessageType)
                {
                    case MessagePacketType.RoomInfo:
                        OnRoomInfoParse(msg);
                        break;
                    case MessagePacketType.Join:
                        OnJoinedParse(msg);
                        break;
                    case MessagePacketType.Full:
                        OnError?.Invoke("a Room is Full");
                        break;
                    case MessagePacketType.Left:
                        OnLeftParse(msg);
                        break;
                    case MessagePacketType.Message:
                        OnMessageParse(msg);
                        break;
                    default:
                        Debug.LogError("::Packet is wrong.... Check Response Data");
                        break;
                }
            };
            await websocket.Connect();
        }

        void OnRoomInfoParse (in string msg)
        {
            var packet= JsonParser.Parse<RoomInfoPacketResp>(msg);
            mine.UID = packet.Mine.UID;
            m_JoinedRoom = packet.Room;
            OnOpen?.Invoke();
        }

        void OnJoinedParse(in string msg)
        {
            var packet = JsonParser.Parse<JoinPacketResp>(msg);
            JoinedRoom.AddUser(packet.User);
            OnJoined?.Invoke(packet.User.UID);
        }

        void OnLeftParse (in string msg)
        {
            var packet = JsonParser.Parse<LeftPackekResp>(msg);
            JoinedRoom.LeftUser(packet.User.UID);
            OnLeft?.Invoke(packet.User.UID);
        }

        void OnMessageParse (in string msg)
        {
            var packet = JsonParser.Parse<MessagePacketResp>(msg);
            var m = MessagePacket.ParseFromMessage(packet.Message);
            if (m.Type != string.Empty)
            {
                m.FromUid = packet.FromUid;
                OnMessage?.Invoke(m);
            } 
            else
            {
                OnMessage?.Invoke(new MessagePacket("NONE", m.Message, m.FromUid));
            }
        }
  
        public void Close()
        {
            websocket?.Close();
            websocket = null;
        }

        async public Task Send(string type, object obj, MessagePacketCastType castType = MessagePacketCastType.All )
        {
            if (websocket != null && websocket.State == WebSocketState.Open)
            {                
                var packet = new MessagePacket(type, JsonParser.ToJson(obj), UID);
                await Send(packet, castType);
            }
        }

        async public Task Send(MessagePacket packet, MessagePacketCastType castType = MessagePacketCastType.All)
        {
            if (websocket != null && websocket.State == WebSocketState.Open)
            {
                var json = JsonParser.ToJson(new MessagePacketReq(message: packet.ToJson(), castType: castType));
                await websocket.SendText(json);
            }
        }

        void Update()
        {
#if !UNITY_WEBGL || UNITY_EDITOR
            websocket?.DispatchMessageQueue();
#endif
        }
    }
}