using Newtonsoft.Json;
using UnityEngine;
using System;

namespace NativeWebSocket.Extentions
{
    public class JsonParser
    {
        static public string ToJson (object obj)
        {
            return JsonConvert.SerializeObject (obj);
        }



        static public T Parse<T>(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch(Exception e) 
            {
                Debug.LogError(e);
                return default;
            }
        }
    }
}
