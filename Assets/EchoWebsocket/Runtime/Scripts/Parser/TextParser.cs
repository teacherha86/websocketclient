namespace NativeWebSocket.Extentions
{
    public class TextParser
    {
        static public string Parse(byte[] bytes)
        {
            var message = System.Text.Encoding.UTF8.GetString(bytes);
            return message;
        }
    }
}