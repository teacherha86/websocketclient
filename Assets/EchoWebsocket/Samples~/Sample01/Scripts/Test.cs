using NativeWebSocket.Extentions;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public Color color;
    struct TestPacket
    {
        public int X;
        public int Y;
    }

    struct ChatPacket
    {
        public string UserName;
        public string Message;
    }

    public enum MyMessageType
    {
        Draw,
        Start,
        End,
    }

    Dictionary<MyMessageType, Action<MessagePacket>> OnMessages = new();


    void OnDrawFunc (MessagePacket msgPacket)
    {
        var packet = msgPacket.ToObject<DrawPacket>();
        ChangeCubeColor(packet.ObjectName, packet.Color);
    }
    void ChangeCubeColor(string objectName, Color changeColor)
    {
        var go = GameObject.Find(objectName);
        if (go != null)
        {
            go.GetComponent<Renderer>().sharedMaterial.color = changeColor;
        }
    }

    private void Awake()
    {
        OnMessages.Add(MyMessageType.Draw, OnDrawFunc);
    }

    private void OnDestroy()
    {
        if(EchoConnection.Instance != null)
            EchoConnection.Instance.Close();
    }

    void Start()
    {
        var echo = EchoConnection.Instance;
        echo.OnOpen += () =>
        {
            Debug.Log("On Opened");
        };
        echo.OnClose += (e) =>
        {
            Debug.Log("On Closed : " + e);
        };
        echo.OnError += (e) =>
        {
            Debug.LogError("On Error : " + e);
        };
        echo.OnJoined += (uid) =>
        {
            Debug.Log(echo.JoinedRoom.JoinedUserLen);
            Debug.Log("On Joined: "+ uid);
        };
        echo.OnLeft += (uid) =>
        {
            Debug.Log("On Left: " + uid);
            Debug.Log(echo.JoinedRoom.JoinedUserLen);
        };
        echo.OnMessage += (msg) => 
        {
            Debug.Log("Get Message: " + msg.Type);
            if (Enum.TryParse<MyMessageType>(msg.Type, out var gotType))
            {
                if (OnMessages.TryGetValue(gotType, out var func))
                    func(msg);
            }

            if (msg.Type == "Ping")
            {
                Debug.Log("Got Ping");
                Debug.Log("from uid : " + msg.FromUid);
                EchoConnection.Instance.Send("Pong", "Pong");
            }


            if (msg.Type == "Pong")
            {
                Debug.Log("from uid : " + msg.FromUid);
                Debug.Log("Got Pong");
            }


            //switch (gotType)
            //{
            //    case MyMessageType.Draw:
            //        break;
            //    case MyMessageType.Start:
            //        break;
            //    default:
            //        Debug.LogError("We don't support type....");
            //        break;
            //}
            //if (msg.Type == "Start")
            //{
            //    Debug.Log("I Got a Start Message");
            //}

                //if (msg.Type == "player")
                //{
                //    var packet = msg.ToObject<TestPacket>();
                //    Debug.Log(packet.X);
                //    Debug.Log(packet.Y);
                //}                        
                //if(gotType == MyMessageType.Draw)
                //{
                //    var packet = msg.ToObject<DrawPacket>();
                //    var go = GameObject.Find(packet.ObjectName);
                //    if(go != null)
                //    {
                //        go.GetComponent<Renderer>().sharedMaterial.color = packet.Color;
                //    }
                //}

                //if(msg.Type == "Chat")
                //{
                //    var packet = msg.ToObject<ChatPacket>();
                //    Debug.Log(packet.UserName);
                //    Debug.Log(packet.Message);
                //}
        };
        echo.Connect();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            EchoConnection.Instance.Send("Start", "");
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            EchoConnection.Instance.Send("GameEnd", "");
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            EchoConnection.Instance.Send(
                "put",
                new TestPacket
                {
                    X = 1,
                    Y = 2,
                }
            );
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            EchoConnection.Instance.Send(MyMessageType.Draw.ToString(), new DrawPacket
            {
                Color = color,
                ObjectName = "Cube"
            }, MessagePacketCastType.Others);
            ChangeCubeColor("Cube", color);
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            EchoConnection.Instance.Send(
                MyMessageType.End.ToString(), new ChatPacket
                {
                    UserName = "Teacher",
                    Message = "hgahaha"
                }                
            );
        }

        if(Input.GetKeyDown(KeyCode.P))
        {
            EchoConnection.Instance.Send("Ping", "Ping");
        }
    }
}
