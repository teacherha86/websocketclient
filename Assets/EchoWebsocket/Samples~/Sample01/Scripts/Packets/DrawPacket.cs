using Newtonsoft.Json;
using UnityEngine;

public struct DrawPacket
{
    public string ObjectName;
    [JsonConverter(typeof(ColorHandler))]
    public Color Color;
}