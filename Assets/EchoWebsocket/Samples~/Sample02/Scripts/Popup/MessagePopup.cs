using System;
using TMPro;
using UnityEngine;


namespace Sample02
{
    public class MessagePopup : MonoBehaviour
    {
        static MessagePopup s_instance;
        static public MessagePopup Instance
        {
            get
            {
                if (s_instance == null)
                    s_instance = GameObject.FindObjectOfType<MessagePopup>(true);
                return s_instance;
            }
        }

        [SerializeField] private TextMeshProUGUI m_Text;
        [SerializeField] private GameObject m_ClosBtn;

        private void OnDestroy()
        {            
            s_instance = null;
        }


        void Awake()
        {    
            if(s_instance == null)
                s_instance = this;
        }

        public void Show (string text)
        {
            if(!gameObject.activeSelf) gameObject.SetActive(true);
            m_Text.text = text;
            m_ClosBtn.SetActive(false);
        }

        public void ShowWithCloseButton(string text)
        {
            if (!gameObject.activeSelf) gameObject.SetActive(true);
            m_Text.text = text;
            m_ClosBtn.SetActive(true);
        }


        public void Hide()
        {
            if (gameObject.activeSelf) gameObject.SetActive(false);
        }
    }
}
