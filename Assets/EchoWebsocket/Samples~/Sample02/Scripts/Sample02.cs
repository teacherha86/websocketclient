using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sample02
{
    public class Sample02 : MonoBehaviour
    {
        [SerializeField] private GameObject m_HomeView;
        [SerializeField] private GameObject m_GameView;


        private void Start()
        {
            m_HomeView.SetActive(true);
            m_GameView.SetActive(false);
        }

    }
}
