using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Sample02
{
    public enum SelectType
    {
        None,
        Rock,
        Scissors,
        Paper,
    }

    public class RockPaperScissors : MonoBehaviour
    {
        [Header("Players")]
        [SerializeField] private RockPaperScissorsPlayer myPlayer;
        [SerializeField] private RockPaperScissorsPlayer anotherPlayer;

        [Header("Timer")]
        [SerializeField] private TextMeshProUGUI m_Timer;

        public void InitBoard()
        {
            myPlayer.Join();            
        }


        public void Play ()
        {
        }

        public void End()
        {
        }

        public void JoinAnotherPlayer ()
        {
            anotherPlayer.Join();
        }        
    }
}
