using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sample02
{

    public class RockPaperScissorsPlayer : MonoBehaviour
    {
        [Header("Sprites")]
        [SerializeField] private Sprite RockSprite;
        [SerializeField] private Sprite ScissorsSprite;
        [SerializeField] private Sprite PaperSprite;

        [Header("ref")]
        [SerializeField] private Image m_Target;

        private SelectType m_CurrentSelectType = SelectType.None;

        Sprite GetSelectSprite (SelectType type)
        {
            return type switch
            {
                SelectType.Rock => RockSprite,
                SelectType.Scissors => ScissorsSprite,
                SelectType.Paper => PaperSprite,
                _ => null
            };
        }

        public void Select(SelectType type)
        {
            m_CurrentSelectType = type;
            m_Target.sprite = GetSelectSprite(type);
        }

        public void Join()
        {
            gameObject.SetActive(true);
        }

    }
}