using System.Threading.Tasks;
using NativeWebSocket.Extentions;
using TMPro;
using UnityEngine;

namespace Sample02
{

    public class Home : MonoBehaviour
    {
        [Header("Title")]
        [SerializeField] private GameObject m_Title;
        
        [Header("Join Input")]
        [SerializeField] private TMP_InputField m_RoomInputField;
        
        [Header("Game View")]
        [SerializeField] private GameObject m_GameView;

        private void OnEnable()
        {
            var echo = EchoConnection.Instance;
            echo.OnOpen += OnOpen;
            echo.OnClose += OnClose;
            echo.OnError += OnError;
        }

        private void OnDisable()
        {
            var echo = EchoConnection.Instance;
            if (echo != null)
            {
                echo.OnOpen -= OnOpen;
                echo.OnClose -= OnClose;
                echo.OnError -= OnError;
            }
        }

        public void OnJoinRoom ()
        {
            var settings = EchoConnection.Instance.Settings;
            settings.RoomId = m_RoomInputField.text;

            EchoConnection.Instance.Connect(settings);
            MessagePopup.Instance.Show("Try to join now....");
        }

        void OnOpen ()
        {
            if (EchoConnection.Instance.IsConnected)
            {
                MessagePopup.Instance.Hide();
                gameObject.SetActive(false);
                m_GameView.SetActive(true);                
            }
        }

        void OnClose (NativeWebSocket.WebSocketCloseCode code)
        {
            MessagePopup.Instance.ShowWithCloseButton("Join Failed.... " + code);
        }

        void OnError (string error)
        {
          //  MessagePopup.Instance.ShowWithCloseButton("Join Failed.... " + error);
        }
    }
}