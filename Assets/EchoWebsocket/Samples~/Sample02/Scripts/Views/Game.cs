using NativeWebSocket.Extentions;
using UnityEngine;
using TMPro;

namespace Sample02
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private GameObject m_HomeView;

        [SerializeField] private TextMeshProUGUI m_RoomId;
        [SerializeField] private RockPaperScissors m_RockPaperScissors;

        private void OnEnable()
        {
            var echo = EchoConnection.Instance;
            echo.OnClose += OnClose;

            InitRockPaperScissors();
        }

        private void OnDisable()
        {
            var echo = EchoConnection.Instance;
            if (echo != null)
            {
                echo.OnClose -= OnClose;
            }
        }

        void InitRockPaperScissors()
        {
            var echo = EchoConnection.Instance;
            m_RoomId.text = echo.JoinedRoom.RoomId;
            m_RockPaperScissors.InitBoard();

            // TODO: Check Users...
        }

        void OnClose(NativeWebSocket.WebSocketCloseCode code)
        {
            if(m_HomeView != null)
                m_HomeView.SetActive(true);            
            if(null != gameObject)
                gameObject.SetActive(false);
        }

        public void OnBack()
        {
            EchoConnection.Instance.Close();
        }
    }
}
