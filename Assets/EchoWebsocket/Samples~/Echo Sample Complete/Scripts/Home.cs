using NativeWebSocket.Extentions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Security;

public class Home : MonoBehaviour
{
    [SerializeField] private TMP_InputField m_RemoteIp;
    [SerializeField] private TMP_InputField m_RoomNumber;
    [SerializeField] private GameObject RockPaperScissors;

    private void Start()
    {
        m_RemoteIp.text = "localhost";
    }

    private void OnEnable()
    {
        var echo = EchoConnection.Instance;
        echo.OnOpen += OnOpen;
        echo.OnClose += OnClose;        
    }

    private void OnDisable()
    {
        var echo = EchoConnection.Instance;
        if(echo != null)
        {
            echo.OnOpen -= OnOpen;
            echo.OnClose -= OnClose;
        }
    }

    void OnOpen()
    {
        Debug.Log("open");
        MessagePopup.Instance.Hide();

        gameObject.SetActive(false);
        RockPaperScissors.SetActive(true);
    }

    void OnClose (NativeWebSocket.WebSocketCloseCode code)
    {
        MessagePopup.Instance.Show("Closed : " + code, true);
    }

    public void OnJoin()
    {
        var settings = EchoConnection.Instance.Settings;
        settings.RemoteIp = $"ws://{m_RemoteIp.text}";
        settings.RoomId = m_RoomNumber.text;
        EchoConnection.Instance.Connect(settings);
        MessagePopup.Instance.Show("Join.... Wait a sec..");
    }

}
