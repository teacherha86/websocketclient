using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MessagePopup : MonoBehaviour
{
    static  MessagePopup instance;

    static public MessagePopup Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<MessagePopup>(true);
            return instance;
        }
    }

    [SerializeField] private TextMeshProUGUI m_Text;
    [SerializeField] private GameObject m_CloseButton;

    

    private void Awake()
    {
        instance = this; 
        GameObject.DontDestroyOnLoad(gameObject);
    }


    public void Show(string message, bool closeButton = false)
    {
        gameObject.SetActive(true);
        m_Text.text = message;
        m_CloseButton.SetActive(closeButton);
    }
    public void Hide() 
    {
        gameObject.SetActive(false);
    }    
}

