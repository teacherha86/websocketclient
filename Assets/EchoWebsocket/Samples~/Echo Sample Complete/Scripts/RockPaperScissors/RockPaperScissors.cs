using NativeWebSocket;
using NativeWebSocket.Extentions;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public enum SelectType
{
    None = 0,
    Rock,
    Scissors,
    Paper,
}

public class RockPaperScissors : MonoBehaviour
{
    [SerializeField] private GameObject m_Home;
    [SerializeField] private GameObject m_PlayerPrefab;


    [Header("Game")]
    [SerializeField] private float m_TimeCount = 5;
    [SerializeField] private GameObject m_Inputs;
    [SerializeField] private TextMeshProUGUI m_Timer;



    private RockPaperScissorsPlayer myPlayer;
    private RockPaperScissorsPlayer anotherPlayer;
    
    private void OnEnable()
    {       
        var echo = EchoConnection.Instance;
        Initialize();
        echo.OnJoined += OnJoinUser;
        echo.OnLeft += OnLeftUser;
        echo.OnClose += OnClose;
        echo.OnMessage += OnMessage;
    }

    private void OnDisable()
    {
        var echo = EchoConnection.Instance;
        if (echo != null)
        {
            echo.OnJoined -= OnJoinUser;
            echo.OnLeft -= OnLeftUser;
            echo.OnClose -= OnClose;
            echo.OnMessage -= OnMessage;
        }
    }

    void OnJoinUser (string userId)
    {
        CreateAnotherPlayer(userId);
    }

    void OnLeftUser (string userId)
    {
        Object.Destroy(anotherPlayer.gameObject);
        GameEnd();
    }

    void OnClose (WebSocketCloseCode code)
    {
        MessagePopup.Instance.Show("Disconnected.... Check your network... or Server");
    }

        
    void ResetGame()
    {
        if(myPlayer != null)
            Object.Destroy (myPlayer.gameObject);
        if(anotherPlayer != null)
            Object.Destroy (anotherPlayer.gameObject);
        myPlayer = null;
        anotherPlayer = null;
    }

    public void Initialize()
    {
        ResetGame();
        // getting user id from server.
        var myUid = EchoConnection.Instance.UID;
        foreach (var user in EchoConnection.Instance.JoinedRoom.Users)
        {
            if(user.UID == myUid)
                myPlayer = CreatePlayer(new Vector3(-200, 0, 0), "My Player", user.UID);
            else
                CreateAnotherPlayer(user.UID);
        }
        m_Timer.gameObject.SetActive(false);
        m_Inputs.SetActive(false);

        SendReady();
    }

    void CreateAnotherPlayer(string uid)
    {
        anotherPlayer = CreatePlayer(new Vector3(200, 0, 0), "Another Player", uid);
    }

    RockPaperScissorsPlayer CreatePlayer(Vector3 localpos, string name, string uid)
    {
        var go = Instantiate(m_PlayerPrefab, transform);
        go.transform.localPosition = localpos;
        go.name = name;
        var player = go.GetComponent<RockPaperScissorsPlayer>();
        player.SetUID(uid);
        return player;
    }

    async public void OnBack()
    {
        MessagePopup.Instance.Show("Closing.....");
        OnDisable();
        EchoConnection.Instance.Close();
        await Task.Delay(1000);
        m_Home.SetActive(true);
        gameObject.SetActive(false);
        MessagePopup.Instance.Hide();

        // Load Scene....
    }



    public void OnInput (int index)
    {
        SelectType type = (SelectType)index;
        EchoConnection.Instance.Send("INPUT", type);
    }

    bool IsAllPlayerReady()
    {
        if (myPlayer != null && anotherPlayer != null)
            return myPlayer.isReady && anotherPlayer.isReady;
        return false;
    }

    void StartGame(string startTime)
    {
        m_Timer.gameObject.SetActive(true);
        m_Timer.text = startTime;
        m_Inputs.gameObject.SetActive(true);
        if (EchoConnection.Instance.IsMaster)
            StartCoroutine(UpdateTimer());
    }


    IEnumerator UpdateTimer ()
    {
        float currentTimer = m_TimeCount;
        while(0 < Mathf.Ceil(currentTimer))
        {          
            currentTimer -= Time.deltaTime;            
            EchoConnection.Instance.Send("TIMER", (Mathf.CeilToInt(currentTimer)));
            yield return null;
        }
        SendTimeout();
    }

    void GameEnd()
    {
        StopAllCoroutines();
        Initialize();        
    }

    void TimeOutRoutine()
    {
        m_Inputs.SetActive(false);
        WhoWon();
    }

    async void WhoWon ()
    {
        var mine = myPlayer.selectType;
        var another = anotherPlayer.selectType;
        // display popup...        

        MessagePopup.Instance.Show("Who Won..??");
        //
        await Task.Delay(1000);
        MessagePopup.Instance.Hide();
        if (EchoConnection.Instance.IsMaster)
        {
            SendStart();
        }
    }

    //------------------------------------------------
    // Below Network code....
    void OnMessage(MessagePacket packet)
    {
        switch (packet.Type)
        {
            case "READY":
                if (packet.FromUid == myPlayer.UID)
                    myPlayer.Ready(packet.ToObject<bool>());
                if (null != anotherPlayer)
                {
                    if (packet.FromUid == anotherPlayer.UID)
                        anotherPlayer.Ready(packet.ToObject<bool>());
                }                
                SendStart();
                return;
            case "START":
                StartGame(packet.ToObject<float>().ToString());
                break;
            case "TIMER":
                m_Timer.text = packet.ToObject<int>().ToString();
                break;
            case "TIMEOUT":
                TimeOutRoutine();
                break;
        }
    }
    void SendReady()
    {
        EchoConnection.Instance.Send("READY", true);
    }
    void SendStart()
    {
        if (EchoConnection.Instance.IsMaster)
        {
            if (IsAllPlayerReady())
            {
                EchoConnection.Instance.Send("START", m_TimeCount);
            }
        }
    }

    void SendTimeout()
    {
        EchoConnection.Instance.Send("TIMEOUT", "");
    }

}
