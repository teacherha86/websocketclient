using NativeWebSocket.Extentions;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RockPaperScissorsPlayer : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI m_Target;

    public string UID;
    public bool isReady = false;

    public SelectType selectType;

    private void OnEnable()
    {
        var echo = EchoConnection.Instance;
        echo.OnMessage += OnMessage;
    }

    private void OnDisable()
    {
        var echo = EchoConnection.Instance;
        if(echo != null)
            echo.OnMessage -= OnMessage;
    }

    void OnMessage (MessagePacket packet)
    {
        switch (packet.Type)
        {
            case "INPUT":
                if(packet.FromUid == UID)
                    Select(packet.ToObject<SelectType>());
                break;
        }
    }

    public void SetUID (string uid)
    {
        UID = uid;
    }

    public void Ready(bool state)
    {
        isReady = true;
    }

    public void Select (SelectType type)
    {
        selectType = type;
        m_Target.text = type.ToString();
    }    

    public void Show()
    {
        m_Target.text = selectType.ToString();
    }
}

