using System.Collections;
using System.Collections.Generic;
using System.Net;
using Unity.VisualScripting;
using UnityEngine;
using Newtonsoft.Json;


public class JsonTest : MonoBehaviour
{
    struct User
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("age")]
        public int Age;
        [JsonProperty("address")]
        public string Address;
    }

    void Start()
    {
        User user = new User
        {
            Name = "teacher",
            Age= 10,
            Address= "My Address"
        };
        var json = JsonConvert.SerializeObject(user, Formatting.Indented);
        Debug.Log(json);

        // string to object
        var converUser = JsonConvert.DeserializeObject<User>(json);
        Debug.Log($"{converUser.Name}, {converUser.Age},{converUser.Address}");
    }
}
